<!--
# READ ME FIRST

This issue is a resource in addition to the general off-boarding task.

Distribution engineering manager is the DRI of this task.

-->

## Team Offboarding Tasks

### Manager

- [ ] Remove the team member from the following AWS account:
  - dev-distribution ([Issue template](https://gitlab.com/gitlab-com/business-technology/engineering/infrastructure/issue-tracker/-/issues/new?issuable_template=aws_group_account_iam_update_request)
- [ ] [Deactivate the team member Red Hat account](https://www.redhat.com/wapps/ugc/protected/usermgt/userList.html)
- [ ] Remove the team member from [ArtifactHub.io](https://artifacthub.io/control-panel/members?page=1)

For access requests, you can submit the access requests by creating a [new issue](https://gitlab.com/gitlab-com/access-requests/issues/new) in gitlab-com/access-requests. Use the New Access Request template for the issue, but this template can be a bit daunting and you can create a comment on this issue to receive some assistance.

/label ~"offboarding"
